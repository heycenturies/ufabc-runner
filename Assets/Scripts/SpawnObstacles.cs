﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacles : MonoBehaviour
{
    
    public GameObject obstacle;
    public float maxX;
    public float minX;
    public float maxY;
    public float minY;
    public float spawnInterval; 
    private float spawnTime;
    private float seconds = 0;
    void Update()
    {
        if(Time.time > spawnTime){
            spawn();
            spawnTime = Time.time + spawnInterval;
        }

        //Enquanto estiver >= 0.4 a dificuldade aumenta a cada 10s
        if(Mathf.Floor(Time.time) % 10 == 0 && Mathf.Floor(Time.time) > seconds && spawnInterval >= 0.4){
            spawnInterval -= (float)0.1;
            seconds = Mathf.Floor(Time.time);;
        }

        //Quando tiver <= 0.3 a dificuldade aumenta a cada 30s
        if(Mathf.Floor(Time.time) % 30 == 0 && Mathf.Floor(Time.time) > seconds && spawnInterval <= 0.4){
            spawnInterval -= (float)0.1;
            seconds = Mathf.Floor(Time.time);;
        }
    }

    void spawn(){
        float randomX = Random.Range(minX,maxX);
        float randomY = Random.Range(minY,maxY);
        Instantiate(obstacle,transform.position + new Vector3(randomX,randomY,0), transform.rotation);
    }
}
